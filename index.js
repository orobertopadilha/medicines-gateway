import express from 'express'
import cors from 'cors'
import handleUserProxy from './proxy/user'
import { authInterceptor } from './middleware/auth'

const app = express()

app.use(cors())
app.use(express.json())
app.use(authInterceptor)
app.use(handleUserProxy)

app.listen(8080, () => console.log('Gateway is up!'))